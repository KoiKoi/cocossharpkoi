﻿using System;
using CocosSharp;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Platforms.CocosSharp.Extensions.Mediation.Impl;
using Robotlegs.Bender.Framework.API;
using System.Collections.Generic;
using System.Diagnostics;
namespace KoiKoi.Common
{

	public class GameMVCConfig : IConfig
	{

		[Inject]
		public IInjector injector;

		[Inject]
		public IMediatorMap mediatorMap;

		public void Configure()
		{

			//Init Model
			CCSpriteView sprite = new CCSpriteView();
			CCRect newRec = sprite.VisibleBoundsWorldspace;
			GameConfigModel gameConfigModel = new GameConfigModel(newRec);
			injector.Map<GameConfigModel>().ToValue(gameConfigModel);

			//Init Views
			mediatorMap.Map<GGameView>().ToMediator<GGameMediator>();


		}
	}
}

