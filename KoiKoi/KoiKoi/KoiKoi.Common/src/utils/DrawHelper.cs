﻿using System;
using CocosSharp;

namespace KoiKoi.Common
{
	public class DrawHelper
	{
		public DrawHelper ()
		{
		}

		public static CCLayer createTileArea (CCPoint areaSize, int tileSize) {
			CCLayer result = new CCLayer ();
			CCDrawNode drawNode = new CCDrawNode();

			CCColor4B color;


			int col = 0;

			for (int x = 0; x < areaSize.X; x+=tileSize) {
				for (int y = 0; x < areaSize.Y; y+=tileSize) {


					if (col % 2 == 1) {
						color = new CCColor4B (0.8F, 0.8F, 0.8F, 1.0F);
					} else {
						color = new CCColor4B (0.6F, 0.6F, 0.6F,  1.0F);
					}

					drawNode.DrawRect(new CCRect(x,y,tileSize,tileSize),color, 1.0F, color);


					col++;

				}
			}


			result.AddChild (drawNode);

			return result;
		}
	}
}

