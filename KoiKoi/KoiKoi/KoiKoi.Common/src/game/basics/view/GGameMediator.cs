﻿using System;
using Robotlegs.Bender.Bundles.MVCS;
using System.Collections.Generic;
using System.Linq;

namespace KoiKoi.Common
{
	public class GGameMediator : Mediator
	{
		[Inject]
		public GGameView view;

		public override void Initialize()
		{
			CocosSharp.CCColor3B color = new CocosSharp.CCColor3B(255, 0, 0);
			view.Color = color;
			base.Initialize();
		}
	}
}

