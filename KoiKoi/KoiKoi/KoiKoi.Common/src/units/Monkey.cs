﻿using System;
using System.Collections.Generic;
using CocosSharp;
using System.Diagnostics;

namespace KoiKoi.Common
{
	public class Monkey : CCNodeGrid
	{

		private CCSprite monkeySprite;

		public Monkey ()
		{
			
		}

		protected override void AddedToScene ()
		{
			initAsset ();
			initTouchlistener ();
			initStartPositions ();
		}

		public void initAsset() {
			monkeySprite = new CCSprite ("monkey");
			monkeySprite.AnchorPoint = CCPoint.AnchorMiddle;
			AddChild (monkeySprite);
		}

		public void initStartPositions() {
			//CCRect visibleBounds = VisibleBoundsWorldspace;
			//CCPoint centerBounds = visibleBounds.Center;
			this.AnchorPoint = CCPoint.AnchorMiddle;
		}


		private void initTouchlistener() {
			var touchListener2 = new CCEventListenerTouchAllAtOnce ();
			touchListener2.OnTouchesEnded = OnTouchesEnded;
			AddEventListener (touchListener2, this);
		}


		public void runToPosition(CCPoint position) {

			CCMoveTo ccMove = new CCMoveTo (6.3f, position);
			CCCallFunc ccFunction = new CCCallFuncN(onCompleteAction);
			CCSequence ccSequence = new CCSequence (ccMove, ccFunction);
			this.RunAction (ccSequence);
		}

		private void onCompleteAction(CCNode mynote)  
		{
			
		}

		private void OnTouchesEnded(List<CCTouch> touches, CCEvent touchEvent) {
			if (monkeySprite.BoundingBoxTransformedToWorld.ContainsPoint (touches [0].Location)) {
				//It works!!
				CCActionState actionState = this.RunAction (new CCRotateTo (0.3f, this.RotationX ++));
			}

		}


	}
}

