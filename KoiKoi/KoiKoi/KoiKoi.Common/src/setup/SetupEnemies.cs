﻿using System;
using System.Collections.Generic;

namespace KoiKoi.Common
{
	public class SetupEnemies {
		
		private Dictionary<EnemyVO.EnemyType, EnemyVO> dictionary = new Dictionary<EnemyVO.EnemyType, EnemyVO>();

		public void initEnemies() {

			saveEnemy(
				new EnemyVO (100, EnemyVO.EnemyType.SMALL_ENEMY)
			);
			
			saveEnemy(
				new EnemyVO (300, EnemyVO.EnemyType.BIG_ENEMY)
			);
		
		}

		private void saveEnemy(EnemyVO enemy) {
			dictionary.Add(enemy.enemyType, enemy);
		}

		public EnemyVO getEnemyByType(EnemyVO.EnemyType type) {
			EnemyVO data = dictionary[type];
			return data;
		}


	}
}

