﻿using System;

namespace KoiKoi.Common
{
	public class EnemyVO
	{
		public enum EnemyType
		{
			SMALL_ENEMY,
			BIG_ENEMY
		}

		public int maxEnergy { get; protected set; }
		public EnemyType enemyType { get; protected set; }

		public EnemyVO (int mEnergym, EnemyType eType)
		{
			maxEnergy = mEnergym;
			enemyType = eType ;
		}
	}
}

