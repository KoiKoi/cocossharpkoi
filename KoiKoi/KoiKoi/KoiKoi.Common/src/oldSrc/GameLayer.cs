﻿using System;
using System.Collections.Generic;
using CocosSharp;
using System.Diagnostics;
using Robotlegs.Bender.Bundles;
using Robotlegs.Bender.Framework.API;
using Robotlegs.Bender.Framework.Impl;

namespace KoiKoi
{
	public class GameLayer : CCLayerGradient
	{
		CCParticleSystem galaxySystem;

		CCNodeGrid monkeySprite1;
		CCNodeGrid monkeySprite2;

		CCRepeatForever repeatedAction;
		CCFiniteTimeAction dreamAction;

		int redColorIncrement = 10;

		private static IContext context;

		public GameLayer (): base (CCColor4B.Blue, CCColor4B.AliceBlue)
		{
			// Set the layer gradient direction
			this.Vector = new CCPoint (0.5f, 0.5f);

			/// Create and add sprites
			// We will later be applying a wave action to these sprites
			// These type of actions can only be applied to CCNodeGrid instances
			// Therefore, wrap our sprites in a CCNodeGrid parent
			monkeySprite1 = new CCNodeGrid ();
			monkeySprite1.AddChild (new CCSprite ("monkey"));
			AddChild (monkeySprite1);

			monkeySprite2 = new CCNodeGrid ();
			monkeySprite2.AddChild (new CCSprite ("monkey"));
			AddChild (monkeySprite2);

			// Define actions
			CCMoveBy moveUp = new CCMoveBy (1.0f, new CCPoint (0.0f, 100.0f));
			CCFiniteTimeAction moveDown = moveUp.Reverse ();

			// A CCSequence action runs the list of actions in ... sequence!
			CCSequence moveSeq = new CCSequence (new CCEaseBackInOut (moveUp), new CCEaseBackInOut (moveDown));

			repeatedAction = new CCRepeatForever (moveSeq);

			// A CCSpawn action runs the list of actions concurrently
			dreamAction = new CCSpawn (new CCFadeIn (5.0f), new CCWaves (5.0f, new CCGridSize (10, 20), 4, 4));

			// Schedule for method to be called every 0.1s
			Schedule (UpdateLayerGradient, 0.1f);
		}

		protected override void AddedToScene ()
		{
			base.AddedToScene ();

			CCRect visibleBounds = VisibleBoundsWorldspace;
			CCPoint centerBounds = visibleBounds.Center;
			CCPoint quarterWidthDelta = new CCPoint (visibleBounds.Size.Width / 4.0f, 0.0f);

			// Layout the positioning of sprites based on visibleBounds
			monkeySprite1.AnchorPoint = CCPoint.AnchorMiddle;
			monkeySprite1.Position = centerBounds + quarterWidthDelta;

			monkeySprite2.AnchorPoint = CCPoint.AnchorMiddle;
			monkeySprite2.Position = centerBounds - quarterWidthDelta;

			// Run actions on sprite
			// Note: we can reuse the same action definition on multiple sprites!
			monkeySprite1.RunAction (new CCSequence (dreamAction, repeatedAction));
			monkeySprite2.RunAction (new CCSequence (dreamAction, new CCDelayTime (0.5f), repeatedAction));

			// Create preloaded galaxy particle system  
			galaxySystem = new CCParticleGalaxy (centerBounds);

			// Customise default behaviour of predefined particle system
			galaxySystem.EmissionRate = 400.0f;
			galaxySystem.EndSize = 3.0f;
			galaxySystem.EndRadius = visibleBounds.Size.Width;
			galaxySystem.Life = 5.0f;
			AddChild (galaxySystem, 0);

			// Create preloaded galaxy particle system  
			galaxySystem = new CCParticleGalaxy (centerBounds);

			// Customise default behaviour of predefined particle system
			galaxySystem.EmissionRate = 400.0f;
			galaxySystem.EndSize = 3.0f;
			galaxySystem.EndRadius = visibleBounds.Size.Width;
			galaxySystem.Life = 5.0f;
			AddChild (galaxySystem, 0);


			var touchListener2 = new CCEventListenerTouchAllAtOnce ();
			touchListener2.OnTouchesEnded = OnTouchesEnded;
			monkeySprite1.AddEventListener (touchListener2, monkeySprite1);

			// Register to touch event
			var touchListener = new CCEventListenerTouchAllAtOnce ();
			touchListener.OnTouchesEnded = OnTouchesEnded;
			AddEventListener (touchListener, this);
		}

		protected void UpdateLayerGradient (float dt)
		{
			CCColor3B startColor = this.StartColor;

			int newRedColor = startColor.R + redColorIncrement;

			if (newRedColor <= byte.MinValue) {
				newRedColor = 0;
				redColorIncrement *= -1;
			} else if (newRedColor >= byte.MaxValue) {
				newRedColor = byte.MaxValue;
				redColorIncrement *= -1;
			}

			startColor.R = (byte)(newRedColor);

			StartColor = startColor;
		}

		void OnTouchesEnded (List<CCTouch> touches, CCEvent touchEvent)
		{
			if (touches.Count > 0) {
				CCTouch touch = touches [0];
				CCPoint touchLocation = touch.Location;
				CCPoint locationOnScreen = touch.Location;

				// Move particle system to touch location
				galaxySystem.StopAllActions ();
				galaxySystem.RunAction (new CCMoveTo (3.0f, touchLocation));

				CCSize ccSize = monkeySprite1.Children.Elements[0].ContentSize;
				CCRect rec = new CCRect(monkeySprite1.PositionX, monkeySprite1.PositionY, ccSize.Width,ccSize.Height);
				Debug.WriteLine ("--------");
				Debug.WriteLine ( "monkeySprite1 width: " + rec.Size.Width + " height: " + rec.Size.Height);
				Debug.WriteLine ( "monkeySprite1 x: " + rec.MinX + " y: " + rec.MinY);
				Debug.WriteLine ( "touch.Location.X: " + locationOnScreen.X + " touch.Location.Y: " + locationOnScreen.Y);

				if (rec.ContainsPoint(locationOnScreen)) {
					monkeySprite1.RunAction (new CCRotateTo (1.0f, monkeySprite1.RotationX + 90));
				//	monkeySprite1.RunAction (new CCSequence (dreamAction));
				}
			
			}
		}
	}
}
