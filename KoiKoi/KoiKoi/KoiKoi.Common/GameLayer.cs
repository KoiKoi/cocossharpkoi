﻿using System;
using System.Collections.Generic;
using CocosSharp;
using System.Diagnostics;
using KoiKoi.Common;
using Microsoft.Xna.Framework;

namespace KoiKoi
{
	public class GameLayer : CCLayerGradient
	{
		
		// Monkey myMonkey;

		MainView mainView;
		CCScene gameScene;

		public GameLayer (CCScene gameScene): base (CCColor4B.Blue, CCColor4B.AliceBlue)
		{
			this.Vector = new CCPoint (0.5f, 0.5f);
			this.gameScene = gameScene;
		}

		protected override void AddedToScene ()
		{
			CCRect visibleBounds = VisibleBoundsWorldspace;
			CCPoint centerBounds = visibleBounds.Center;

			mainView = new MainView (visibleBounds);
		}

	}
}
