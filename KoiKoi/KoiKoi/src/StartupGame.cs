﻿using System;
using System.Collections.Generic;
using UIKit;
using CocosSharp;

using Robotlegs.Bender.Framework.Impl;
using Robotlegs.Bender.Bundles.MVCS;

namespace KoiKoi.iOS
{
	public class StartupGame:AppDelegate
	{
		private CCGameView gameView;
		private Context context;

		public StartupGame(CCGameView  gameView)
		{
			this.gameView = gameView;
			CCScene gameScene = new CCScene(gameView);
			GameLayer gameLayer = new GameLayer(gameScene);
			gameScene.AddLayer(gameLayer);
			gameView.RunWithScene(gameScene);


			context = new Context();
			context.Install<MVCSBundle>();
			context.Configure<KoiKoi.Common.GameMVCConfig>();
			context.Configure<KoiKoi.Common.AddScenesConfig>();

			context.injector.Map<GameLayer>().ToValue(gameLayer);
		}

		public void setupAppliction(UIApplication application, UIWindow window)
		{
			context.injector.Map<UIApplication>().ToValue(application);
			context.injector.Map<UIWindow>().ToValue(window);
			context.Initialize();
		}


	}
}

